require 'rails_helper'

describe 'home index' do
  before(:each) do
    visit home_index_path
  end

  it 'shows home index page' do
    expect(current_path).to eq home_index_path
  end

  it 'shows home index page' do
    expect(page).to have_content('Email Delivery. Simplified.')
  end
end
