class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def get_header
    require 'nokogiri'
    require 'open-uri'

    page = Nokogiri::HTML(open("http://www.solidfire.com/"))
    page.css("header").text
  end
end
