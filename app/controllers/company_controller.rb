class CompanyController < ApplicationController
  def index
  end

  def board
    set_meta_tags :title => 'SolidFire | Our Board',
                  :description => 'SolidFire Board of Directors bring a combined 150 years of technologically-focused entrepreneurial spirit to the SolidFire team.',
                  :canonical => 'http://www.solidfire.com/about/board/'
  end
end
