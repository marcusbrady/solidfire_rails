class PressesController < ApplicationController
  before_action :set_press, only: [:show, :edit, :update, :destroy]

  # GET /presses
  def index
    @presses = Press.all
  end

  # GET /presses/1
  def show
  end

  # GET /presses/new
  def new
    @press = Press.new
  end

  # GET /presses/1/edit
  def edit
  end

  # POST /presses
  def create
    @press = Press.new(press_params)

    # uploader = PressUploader.new

    # uploader.store!(params[:press][:logo])

    removeFileExtension = params[:press][:logo_image_name][0..-5]

    Cloudinary::Uploader.upload(params[:press][:logo], :public_id => 'press/' + removeFileExtension)

    if @press.save
      redirect_to @press, notice: 'Press was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /presses/1
  def update
    if @press.update(press_params)
      redirect_to @press, notice: 'Press was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /presses/1
  def destroy
    @press.destroy
    redirect_to presses_url, notice: 'Press was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_press
      @press = Press.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def press_params
      params.require(:press).permit(:title, :summary, :url, :logo_image_name)
    end
end
