$(function(){
  $('#q').on('blur',function(){
    $('.header-nav').removeClass('search-active');
  });
  $('.js-q-toggle').on('mousedown',function(event){
    event.preventDefault();var form=$('.header-nav');
    if(!form.hasClass('search-active')){
      setTimeout(function(){document.getElementById('q').focus();
      },50);
      form.addClass('search-active');
     } else {
       form.removeClass('search-active');
     }
  });

  $('.mobile-menu').click(function(){
    $('body').toggleClass('show-mobile-menu');
    $('.mobile-menu-toggle').toggleClass('active');
    $('footer').toggleClass('active');
    $('.footer-search').toggle();

    // scroll to the top of the page when mobile menu is clicked
    window.scrollTo(0, 0);

    if ($('#top-footer-links').css('display') == 'none') {
      $('#top-footer-links').toggle();
    }
  });

  $("label[for='query-footer']").click(function(){
    $('form#footer-search-form').submit();
  });

});