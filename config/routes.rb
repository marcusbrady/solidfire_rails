Rails.application.routes.draw do
  get 'company/index'

  get 'resources/index'

  resources :presses

  # route the site root to home index
  root :to => 'home#index'

  match '/', :to => "home#index", :as => :home_index, :via => :get
  match '/home', :to => "home#home", :as => :home, :via => :get

  match '/resources', :to => "resources#index", :as => :resources, :via => :get

  match '/about/board', :to => "company#board", :as => :board, :via => :get

end
