# README #

The solidfire_rails repo is the marketing frontend of SolidFire. This project is built on rails 4.2.0. Below you will find instructions for getting this repo setup on your local machine and for pushing code to our development and production servers.

### How do I get set up? ###

* Clone this repo to your local machine. git clone git@bitbucket.org:solidfire/solidfire_rails.git
* Download and install the [pow web server](http://pow.cx/)
* Run bundle install in the root directory of solidfire_rails repo on your local machine.
* Browse to [http://solidfire_rails.dev/](http://solidfire_rails.dev/) and the rails app should load.

### Pushing to Development and Production ###

* Set up the git remotes in the root directory of the solidfire_rails repo.
  - git remote add staging https://git.heroku.com/solidfire-development.git
  - git remote add production https://git.heroku.com/solidfire-cloudfront.git
  - git remote add origin git@bitbucket.org:solidfire/solidfire_rails.gi

* Before you can push to development and production you need to download and install the heroku toolbelt app. You will need a login and perms for pushing. Talk to me first and I'll go over this part.

* Once setup on heroku you can push your code up to development and production. Only execute a deploy once your code has been reviewed and merged into the master branch.

* To pull down the latest changes from the master branch use the following command: git pull origin master. This will pull down the latest changes and merge them into the current branch that you have checked out on your local machine.

* Command to push to development: git push development dev
* Command to push to production: git push production master

* Tail log files on development: heroku logs -t --app solidfire-development
* Tail log files on production: heroku logs -t --app solidfire-cloudfront